## Starting algorithm

Application is build using laravel 5.6. Before you start working with it, please follow this steps:


1. In the project directory run "composer install" and "npm init". That will install all required dependencies.

2. In the root folder of the project create ".env" file with all configurations. You can download example ".env" file from any default laravel project.

3. Paste all your configurations into ".env" file (basically database and mail agent).

4. Run "php artisan key:generate" take generated key and paste it into ".env/APP_ENV".

5. Run "php artisan migrate".

6. Run "php artisan db:seed". That will create default admin user with simple bcrypted password and email. 
You will need them for entering into your application for the first time. Later you can change it.

7. You can run application by "php artisan serve" command.


## Basic functionality


1. Admin and user templates are divided and guarded.

2. Good starting steps will be adding new locations and new permissions (required for adding request by user). You can edit and delete them.

3. Admin can add new users, delete them and edit users information. User will automatically get information letter on him being registered or any decision upon his request. 

4. In list of requests you can filter them, approve or decline.  

5. Employee can add requests or delete them unless they were processed.


Feel free to contact me if you encounter any difficulties. 


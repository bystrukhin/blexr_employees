@extends('adminlte::page')
@section('title', 'Blexr employees')
@section('content')

        @foreach($users as $user)
            <div class="container-users">
                <div class="card-header">
                    <h2>
                        {{$user->name}}
                    </h2>
                </div>
                <ul>Permissions:
                    @foreach($user->permissions as $permission)
                        <li>{{$permission->name}}</li>
                    @endforeach
                </ul>
                <br>
                <div class="container-edit-users">
                    <form action="{{route('admin.editUser', ['user_id' => $user->id])}}">
                        <button class="btn btn-primary" type="submit">Edit</button>
                    </form>
                    <form action="{{route('admin.deleteUser', ['user_id' => $user->id])}}">
                        <button class="btn btn-primary" type="submit">Delete</button>
                    </form>
                </div>
            </div>

        @endforeach

@endsection
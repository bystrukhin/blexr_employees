<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<h1>Hello {{ $user->name }}!</h1>

<div>You've been added to our request api. Now you can use it for making requests for home working days! </div>
<div>Credentials for entering system are your email and password: {{$code}}.</div>
<div>The address of the site <a href="{{url("/signin")}}">{{url("/signin")}}</a></div>

<div>There are few limits you should remember:</div>

<ul>
    <li>request has to be made at least 8 hours before the end of the previous day</li>
    <li>if you are sick, a request can be made by 8am of that same working day</li>
    <li>also you can delete your requests before they are processed</li>
</ul>

<div>
    Your sincerely,
</div>

<div>
    Blexr administration
</div>

</body>
</html>
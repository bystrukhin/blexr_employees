@extends('adminlte::page')
@section('title', 'Blexr employees')
@section('content')

        @foreach($requests as $request)
            <div class="container-user-requests">
                @if($request->approval == null)
                    <p>
                        Date: {{$request->date}}
                    </p>
                    {{--<p>--}}
                    {{--Location: {{ $request->locations->address }}--}}
                    {{--</p>--}}
                    </div>

                    <form action="{{route('admin.delete_user_request', ['request_id' => $request->id])}}">
                        <button class="btn btn-primary" type="submit">Delete</button>
                    </form>
                @endif

        @endforeach

@endsection
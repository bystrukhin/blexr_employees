@extends('adminlte::page')
@section('title', 'Blexr employees')
@section('content')
    @include('partials.search')
        @foreach($requests as $request)
            <div class="container-requests">
                <div class="card-header">
                    <h2>
                        {{$request->users->name}}
                    </h2>
                </div>
                    <div class="col-md-8">Dates: {{$request->date}}</div>

                    @if($request->approval !== null)
                        <div class="col-md-8">Status: {{$request->approval}}</div>
                    @endif
                    @if($request->approval == null)
                        <div class="container-edit-requests">
                            <form action="{{route('admin.request_approval', ['id' => $request->id])}}">
                                <input type="hidden" name="decision" value="approved">
                                <button class="btn btn-primary" type="submit">Approve</button>
                            </form>
                            <form action="{{route('admin.request_approval', ['id' => $request->id])}}">
                                <input type="hidden" name="decision" value="declined">
                                <button class="btn btn-primary" type="submit">Decline</button>
                            </form>
                        </div>
                    @endif
            </div>
        @endforeach


@endsection
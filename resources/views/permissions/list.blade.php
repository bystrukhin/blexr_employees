@extends('adminlte::page')
@section('title', 'Blexr employees')
@section('content')


        @foreach($permissions as $permission)
            <div class="container-permissions">
                <div class="card-header">
                    <h2>
                        {{$permission->name}}
                    </h2>
                </div>
                <div class="container-edit-permissions">
                    <form action="{{route('admin.editPermission', ['permission_id' => $permission->id])}}">
                        <button class="btn btn-primary" type="submit">Edit</button>
                    </form>
                    <form action="{{route('admin.deletePermission', ['permission_id' => $permission->id])}}">
                        <button class="btn btn-primary" type="submit">Delete</button>
                    </form>
                </div>
            </div>
        @endforeach


@endsection
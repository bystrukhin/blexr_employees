@extends('adminlte::page')
@section('title', 'Blexr employees')
@section('content')


        <form method="post" action="{{route('admin.postEditPermission', ['permission_id'=>$permission->id])}}">
            <div class="form-element">
                <label for="name">Permission name</label>
                <input type="text" name="name" id="name" value="{{$permission->name}}">
            </div>
            <br>
            <button type="submit" class="btn btn-primary">Edit permission</button>
            {{ csrf_field() }}
        </form>


@endsection
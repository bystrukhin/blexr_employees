@extends('adminlte::page')
@section('title', 'Blexr employees')
@section('content')

    @foreach($locations as $location)
        <div class="container-locations">
            <div class="card-header">
                <h2>
                    {{$location->address}}
                </h2>
            </div>
            <div class="container-edit-locations">
                <form action="{{route('admin.editLocation', ['location_id' => $location->id])}}">
                    <button class="btn btn-primary" type="submit">Edit</button>
                </form>
                <form action="{{route('admin.deleteLocation', ['location_id' => $location->id])}}">
                    <button class="btn btn-primary" type="submit">Delete</button>
                </form>
            </div>
        </div>
    @endforeach

@endsection
@extends('adminlte::page')
@section('title', 'Blexr employees')
@section('content')

    @foreach($user as $item)
        <form method="post" action="{{route('admin.postCredentialsChange', ['user_id'=>$item->id])}}">
            <div class="container-admin-credentials">
                <div class="form-element">
                    <label for="email">Name</label>
                    <input type="text" name="name" id="name" value="{{$item->name}}">
                </div>
                <br>
                <br>
                <div class="form-element">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" value="{{$item->email}}">
                </div>
                <br>
                <br>
                <div class="input-group">
                    <label for="password">Your new password</label>
                    <input name="password" type="password" class="form-control" value="" id="password"/>
                </div>
                <div class="input-group">
                    <label for="password_confirmation">Confirm your new password</label>
                    <input name="password_confirmation" type="password" class="form-control" value="" id="password_confirmation"/>
                </div>
                <span id="helpBlock" class="help-block">If no change, to password leave blank.</span>
            </div>
            <button type="submit" class="btn btn-primary">Edit credentials</button>
            {{ csrf_field() }}
        </form>
    @endforeach

@endsection